<?php

namespace Database\Seeders;

use App\Models\CourseType;
use Illuminate\Database\Seeder;

class CourseTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CourseType::create(
            [
                'name' => 'Testing',
                'description' => 'Learning about testing software and developing quality code'
            ]
        );
        CourseType::create(
            [
                'name' => 'Databases',
                'description' => 'Learning about creating databases and developing database structure'
            ]
        );

        CourseType::create(
            [
                'name' => 'Developing of large systems',
                'description' => 'Learning about develop large scale systems'
            ]
        );

    }
}
