<?php

namespace Database\Seeders;

use App\Models\Course;
use Illuminate\Database\Seeder;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Course::create(
            [
                'course_type_id' => 2,
                'start_time' => '2021-04-10 09:00:00',
                'end_time' => '2021-04-10 13:00:00',
                'team_id' => 1,
                'teacher_id' => 1
            ]
        );

        Course::create(
            [
                'course_type_id' => 1,
                'start_time' => '2021-04-11 13:30:00',
                'end_time' => '2021-04-11 16:00:00',
                'team_id' => 2,
                'teacher_id' => 1
            ]
        );

        Course::create(
            [
                'course_type_id' => 1,
                'start_time' => '2021-04-11 13:30:00',
                'end_time' => '2021-04-11 16:00:00',
                'team_id' => 1,
                'teacher_id' => 2
            ]
        );

        Course::create(
            [
                'course_type_id' => 1,
                'start_time' => '2021-04-11 09:00:00',
                'end_time' => '2021-04-11 13:00:00',
                'team_id' => 1,
                'teacher_id' => 1
            ]
        );

        Course::create(
            [
                'course_type_id' => 1,
                'start_time' => '2021-04-12 13:00:00',
                'end_time' => '2021-04-12 19:00:00',
                'team_id' => 1,
                'teacher_id' => 1
            ]
        );

    }
}
