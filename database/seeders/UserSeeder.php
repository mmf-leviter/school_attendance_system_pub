<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create(
            [
                'name' => 'Admin',
                'phone_number' => '23232323',
                'email' => 'admin@mail.com',
                'password' => Hash::make('default'),
                'api_token' => Str::random(60)
            ]);
        $user->assignRole('admin');

        $user = User::create(
            [
                'name' => 'Mathias',
                'phone_number' => '23232323',
                'email' => 'test@mail.com',
                'password' => Hash::make('test12345'),
                'api_token' => Str::random(60)
            ]);
        $user->assignRole('teacher');
        $user->teacherStudyTeams()->attach([1]);

        $user = User::create(
            [
                'name' => 'Torben',
                'phone_number' => '23232323',
                'email' => 'testt@mail.com',
                'password' => Hash::make('test12345'),
                'api_token' => Str::random(60)
            ]);
        $user->assignRole('teacher');
        $user->teacherStudyTeams()->attach([2]);

        $user = User::create(
            [
                'name' => 'Alper',
                'phone_number' => '23232323',
                'email' => 'test1@mail.com',
                'password' => Hash::make('test12345'),
                'api_token' => Str::random(60)
            ]);
        $user->assignRole('student');
        $user->studentStudyTeams()->attach([1]);

        $user = User::create(
            [
                'name' => 'August',
                'phone_number' => '23232323',
                'email' => 'test2@mail.com',
                'password' => Hash::make('test12345'),
                'api_token' => Str::random(60)
            ]);
        $user->assignRole('student');
        $user->studentStudyTeams()->attach([1]);

        $user = User::create(
            [
                'name' => 'Anton',
                'phone_number' => '23232323',
                'email' => 'test3@mail.com',
                'password' => Hash::make('test12345'),
                'api_token' => Str::random(60)
            ]);
        $user->assignRole('student');
        $user->studentStudyTeams()->attach([1]);

        $user = User::create(
            [
                'name' => 'Osvald',
                'phone_number' => '23232323',
                'email' => 'test4@mail.com',
                'password' => Hash::make('test12345'),
                'api_token' => Str::random(60)
            ]);
        $user->assignRole('student');
        $user->studentStudyTeams()->attach([1]);

    }
}
