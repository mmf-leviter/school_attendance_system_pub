<?php

namespace Database\Seeders;

use App\Models\StudyTeam;
use Illuminate\Database\Seeder;

class StudyTeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StudyTeam::create(
            [
               'name' => 'SW001'
            ]
        );
        StudyTeam::create(
            [
                'name' => 'SW002'
            ]
        );
    }
}
