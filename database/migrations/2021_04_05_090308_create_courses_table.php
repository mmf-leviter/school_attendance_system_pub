<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('course_type_id');
            $table->foreign('course_type_id')->references('id')->on('course_types')->onDelete('cascade');

            $table->dateTime('start_time')->nullable(false);
            $table->dateTime('end_time')->nullable(false);

            $table->string('course_ip_address')->nullable(true);
            $table->integer('time_limit')->default(600);

            $table->unsignedBigInteger('team_id');
            $table->foreign('team_id')->references('id')->on('study_teams')->onDelete('cascade');

            $table->unsignedBigInteger('teacher_id');
            $table->foreign('teacher_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
