<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Course
 *
 * @property int $id
 * @property int $course_type_id
 * @property string $start_time
 * @property string $end_time
 * @property int $team_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\CourseType $courseType
 * @property-read \App\Models\StudyTeam $studyTeam
 * @method static \Illuminate\Database\Eloquent\Builder|Course newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Course newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Course query()
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereCourseTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereUpdatedAt($value)
 */
	class Course extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CourseAttendance
 *
 * @property string $id
 * @property string $attendance_key
 * @property int $time_limit
 * @property int $confirmed
 * @property string $started_at
 * @property string|null $completed_at
 * @property int $student_id
 * @property int $course_id
 * @property-read \App\Models\Course $course
 * @property-read \App\Models\User $student
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance query()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance whereAttendanceKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance whereCompletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance whereConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance whereStartedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance whereTimeLimit($value)
 */
	class CourseAttendance extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CourseType
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Course[] $courses
 * @property-read int|null $courses_count
 * @method static \Illuminate\Database\Eloquent\Builder|CourseType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseType query()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseType whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseType whereName($value)
 */
	class CourseType extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\StudyTeam
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Course[] $courses
 * @property-read int|null $courses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $students
 * @property-read int|null $students_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $teachers
 * @property-read int|null $teachers_count
 * @method static \Illuminate\Database\Eloquent\Builder|StudyTeam newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StudyTeam newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StudyTeam query()
 * @method static \Illuminate\Database\Eloquent\Builder|StudyTeam whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StudyTeam whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StudyTeam whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StudyTeam whereUpdatedAt($value)
 */
	class StudyTeam extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $phone_number
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StudyTeam[] $studentStudyTeams
 * @property-read int|null $student_study_teams_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StudyTeam[] $teacherStudyTeams
 * @property-read int|null $teacher_study_teams_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

