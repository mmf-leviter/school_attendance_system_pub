<?php

use App\Http\Controllers\Api\CourseAttendanceController;
use App\Http\Controllers\Api\CourseController;
use App\Http\Controllers\Api\CourseTypeController;
use App\Http\Controllers\Api\ImportController;
use App\Http\Controllers\Api\StudentController;
use App\Http\Controllers\Api\StudyTeamController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'auth:api'],function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::group(['as'=>'api.'], function(){

        /*
        |--------------------------------------------------------------------------
        | Roles: Admin & Teacher
        |--------------------------------------------------------------------------
        */
        Route::group(['middleware' => ['role:admin|teacher']], function () {
            //Students
            Route::get('/students', [StudentController::class, 'index'])->name('students');


            //Students
            Route::get('/teachers', [\App\Http\Controllers\Api\TeacherController::class, 'index'])->name('teachers');
            Route::get('/students-by-teacher', [StudentController::class, 'studentsByTeacher'])->name('students.by-teacher');

            //Study Teams
            Route::get('/study-teams', [StudyTeamController::class, 'index'])->name('studyTeams');
            Route::get('/study-teams/by-teacher', [StudyTeamController::class, 'allByTeacher'])->name('studyTeams.allByTeacher');

            Route::group(['prefix' => 'study-team', 'as' => 'study-team.'], function () {
                Route::get('/{id}', [StudyTeamController::class, 'show'])->name('show');
                Route::prefix('{id}')->group(function () {
                    Route::get('/teachers', [\App\Http\Controllers\Api\TeacherController::class, 'byTeam'])->name('teachers');

                });

            });

            //Courses
            Route::get('/courses', [CourseController::class, 'index'])->name('courses');
            Route::get('/courses/by-teacher', [CourseController::class, 'allByTeacher'])->name('courses.allByTeacher');

            Route::group(['prefix' => 'course', 'as' => 'course.'], function () {
                Route::post('/store', [CourseController::class, 'store'])->name('store');
                Route::group(['prefix' => '{id}'], function () {
                    Route::get('/get', [CourseController::class, 'show'])->name('show');
                    Route::put('/update', [CourseController::class, 'update'])->name('update');
                    Route::delete('/delete', [CourseController::class, 'destroy'])->name('delete');
                });
                Route::get('/team/{teamId}', [CourseController::class, 'byTeam'])->name('byTeam');

            });

            //Course Types
            Route::get('/course-types', [CourseTypeController::class, 'index'])->name('course-types');
            Route::group(['prefix' => 'course-type', 'as' => 'course-type.'], function () {
                Route::group(['prefix' => '{id}'], function () {
                    Route::get('/get', [CourseTypeController::class, 'show'])->name('get');

                });
            });

            Route::get('/attendance-statistics', [CourseAttendanceController::class, 'getStatisticAttendanceDataByTeam'])->name('attendance-statistics');
        });
        /*
         |--------------------------------------------------------------------------
         | Roles: Admin
         |--------------------------------------------------------------------------
         */

        Route::group(['middleware' => ['role:admin']], function () {
            Route::post('/import-users', [ImportController::class, 'importUsers'])->name('import-users');;
        });
        /*
         |--------------------------------------------------------------------------
         | Roles: Students
         |--------------------------------------------------------------------------
         */
            //Course attendances
            Route::get('/course-attendances', [CourseAttendanceController::class, 'index'])->name('course-attendances')->middleware(['role:teacher|admin']);
            Route::get('/student-attendances', [CourseAttendanceController::class, 'studentAttendances'])->name('student-attendances')->middleware(['role:student']);
            Route::group(['prefix' => 'course-attendance', 'as' => 'course-attendance.'], function () {
                Route::group(['prefix' => '{id}'], function () {
                    Route::get('/get', [CourseAttendanceController::class, 'get'])->name('get')->middleware(['role:student']);
                    Route::put('/update', [CourseAttendanceController::class, 'update'])->name('update')->middleware(['role:student|teacher']);

                });
                Route::post('/start', [CourseAttendanceController::class, 'createAttendances'])->name('start')->middleware(['role:admin|teacher']);

            });
    });


});


