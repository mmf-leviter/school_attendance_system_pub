<?php

use App\Http\Controllers\ImportController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\CourseAttendanceController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\StudyTeamController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::user()) {
        return redirect('home');
    }
    return view('welcome');
});

Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [LoginController::class, 'login']);
Route::post('logout', [LoginController::class, 'logout'])->name('logout');
//Auth::routes();
Route::group(['middleware' => 'auth:web'],function () {
    Route::group(['middleware' => ['role:admin']],function () {
        Route::get('/register', [RegisterController::class, 'showRegistrationForm'])->name('register');
        Route::post('register', [RegisterController::class, 'register']);
        Route::get('/import-users', [ImportController::class, 'importUsersForm'])->name('import-users');
    });


    Route::get('/home', [HomeController::class, 'index'])->name('home');


    Route::get('/students', [StudentController::class, 'index'])->name('students');
    Route::prefix('student.')->group(function () {
    });

    Route::get('/study-teams', [StudyTeamController::class, 'index'])->name('studyTeams');
    Route::group(['prefix' => 'study-team', 'as' => 'study-team.'], function () {
        Route::get('/{id}', [StudyTeamController::class, 'show'])->name('show');
    });


    Route::get('/courses', [CourseController::class, 'index'])->name('courses');
    Route::group(['prefix' => 'course', 'as' => 'course.'], function () {
        Route::get('/{id}', [CourseController::class, 'show'])->name('show');
    });

    Route::get('/attendances', [CourseAttendanceController::class, 'index'])->name('attendances');
    Route::group(['prefix' => 'attendance', 'as' => 'attendance.'], function () {
        Route::get('/{id}', [CourseAttendanceController::class, 'show'])->name('show');
    });

    Route::get('/teacher-stats', [\App\Http\Controllers\TeacherStatsController::class, 'index'])->name('teacher-stats');
});


