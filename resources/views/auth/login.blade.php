@extends('layouts.auth')

@section('content')
<div class="container text-white">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card border-0" style="background-color: #154f9d">
                <div class="card-body">
                    <h1 class="card-title text-center text-white" >{{ __('Login') }}</h1>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group row justify-content-center">
                            <div class="col-md-10">
                                <input id="email" type="email" placeholder="Email"  class="transparent-input text-white form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row justify-content-center ">
                            <div class="col-md-10">
                                <input id="password" type="password" placeholder="Password" class="border-0 transparent-input text-white form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-1">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center">
                            <div class="col-md-10">
                                <button type="submit" class="btn btn-lg btn-primary rounded-pill w-100">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div>
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link text-white" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
