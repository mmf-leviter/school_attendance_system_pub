<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- CoreUI CSS -->

    <link rel="stylesheet" href="https://unpkg.com/@coreui/coreui/dist/css/coreui.min.css" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/4a5876fdef.js" crossorigin="anonymous"></script>
    <script src=""></script>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style src="vue-multiselect/dist/vue-multiselect.min.css"></style>

    <title>{{ config('app.name', 'School management') }}</title>
</head>
<body class="c-app">
@include('partials.menu')
@include('partials.wrapper')

<!-- Optional JavaScript -->
<!-- Popper.js first, then CoreUI JS -->
<script src="https://cdn.jsdelivr.net/npm/@morioh/v-perfect-scrollbar@latest/dist/scrollbar.min.js" type="text/javascript"></script>

<script src="https://unpkg.com/@popperjs/core@2"></script>
<script src="{{ asset('js/coreUI.js') }}" defer></script>
<script>
    window.Laravel = {!! json_encode([
       'csrfToken' => csrf_token(),
       'apiToken' => Auth::user()->api_token ?? null,
   ]) !!};

</script>
</body>
</html>
