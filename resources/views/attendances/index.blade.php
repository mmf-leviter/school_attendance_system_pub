@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        @if($errors->any())
            <div class="alert alert-danger" role="alert">
                {{$errors->first()}}
            </div>
        @endif
        <attendances-list attendances-fetch-endpoint="{{$attendancesFetchEndpoint}}" attendance-view-endpoint="{{$attendanceViewEndpoint}}"></attendances-list>
    </div>
@endsection
