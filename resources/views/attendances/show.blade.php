@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <attendance-view attendance-fetch-endpoint="{{$attendanceFetchEndpoint}}" attendance-update-endpoint="{{$attendanceUpdateEndpoint}}" ></attendance-view>
    </div>
@endsection
