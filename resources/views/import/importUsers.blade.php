@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <import-excel title="Import users from Excel File" import-endpoint="{{$userImportEndpoint}}"></import-excel>
    </div>
</div>
@endsection
