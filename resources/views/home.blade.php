@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @auth
                    @if(Auth::user()->hasRole(['teacher']))
                        <teacher-dashboard teacher-id="{{json_encode($teacher->id)}}"
                                           course-view-endpoint="{{$courseViewEndpoint}}"
                                           students-by-teacher-endpoint="{{$studentsByTeacherEndpoint}}"
                                           courses-by-teacher-endpoint="{{$coursesByTeacherEndpoint}}"/>
                    @endif
                    @if(Auth::user()->hasRole(['student']))
                        <student-dashboard/>
                    @endif
                        @if(Auth::user()->hasRole(['admin']))
                            <h1>admin</h1>
                        @endif
                @endauth
            </div>
        </div>
    </div>
@endsection
