@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <courses-list  courses-fetch-endpoint="{{$coursesFetchEndpoint}}" course-view-endpoint="{{$courseViewEndpoint}}" ></courses-list>
    </div>
@endsection
