@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <course-view :user="{{json_encode($user)}}" attendance-update-endpoint="{{$attendanceUpdateEndpoint}}" course-delete-endpoint="{{$courseDeleteEndpoint}}" course-update-endpoint="{{$courseUpdateEndpoint}}" course-id="{{$courseId}}" course-fetch-endpoint="{{$courseFetchEndpoint}}" attendance-start-endpoint="{{$attendanceStartEndpoint}}" ></course-view>
    </div>
@endsection
