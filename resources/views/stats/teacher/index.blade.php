@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <teacher-stats course-types-fetch-endpoint="{{$courseTypesFetchEndpoint}}" attendance-statistics-fetch-endpoint="{{$attendanceStatisticsFetchEndpoint}}" team-fetch-endpoint="{{ $teamFetchEndpoint }}"></teacher-stats>
    </div>
@endsection
