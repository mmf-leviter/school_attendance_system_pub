@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <students-list students-fetch-endpoint="{{$studentsFetchEndpoint}}" ></students-list>
    </div>
@endsection
