<div class="c-wrapper c-fixed-components">
    <header class="c-header c-header-light c-header-fixed c-header-with-subheader">
        <button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" >
            <i class="fas fa-bars"></i>
        </button>
    </header>
    <div class="c-body">
        <main class="c-main">
            <div class="container-fluid">
                <div id="app">
                    @yield('content')
                </div>
            </div>
        </main>
        <footer class="c-footer">
            <div><a href="https://leviter.dk">Leviter</a> © 2020 Leviter.</div>
        </footer>
    </div>
</div>
