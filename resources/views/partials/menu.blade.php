<div class="c-sidebar c-sidebar-dark c-sidebar-lg-show" id="sidebar">
    <div class="c-sidebar-brand d-lg-down-none">
        @if(Auth::user())
            {{ Auth::user()->name }}
        @else
            {{ config('app.name', 'Laravel') }}
        @endif

    </div>
    <ul class="c-sidebar-nav">
        @auth
            @if(Auth::user()->hasRole(['teacher']))
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="{{ route('home') }}">
                        Dashboard
                    </a>
                </li>
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="{{ route('teacher-stats') }}">
                        Stats
                    </a>
                </li>
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="{{ route('students') }}">
                        Students
                    </a>
                </li>
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="{{ route('studyTeams') }}">
                        Study Teams
                    </a>
                </li>
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="{{ route('courses') }}">
                        Courses
                    </a>
                </li>
            @endif
            @if(Auth::user()->hasRole(['admin']))
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="{{ route('home') }}">
                        Dashboard
                    </a>
                </li>
                <li class="c-sidebar-nav-item c-sidebar-nav-dropdown c-show">
                    <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#"><i class="fas fa-users-cog p-2"></i>Register users</a>
                    <ul class="c-sidebar-nav-dropdown-items">
                        <li class="c-sidebar-nav-item">
                            <a class="c-sidebar-nav-link" href="{{ route('register') }}">
                                <span class="c-sidebar-nav-icon"></span> <i class="fas fa-user-plus p-2"></i> Single user
                            </a>
                        </li>
                        <li class="c-sidebar-nav-item">
                            <a class="c-sidebar-nav-link" href="{{ route('import-users') }}">
                                <span class="c-sidebar-nav-icon"></span><i class="far fa-file-excel p-2"></i> Import users
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="{{ route('students') }}">
                        Students
                    </a>
                </li>
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="{{ route('studyTeams') }}">
                        Study Teams
                    </a>
                </li>
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="{{ route('courses') }}">
                        Courses
                    </a>
                </li>
            @endif

            @if(Auth::user()->hasRole(['student']))
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="{{ route('home') }}">
                        Dashboard
                    </a>
                </li>
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link" href="{{ route('attendances') }}">
                        Attendances
                    </a>
                </li>
            @endif

            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="/"
                   onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
            </li>
        @endauth
    </ul>
</div>
<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
    @csrf
</form>
