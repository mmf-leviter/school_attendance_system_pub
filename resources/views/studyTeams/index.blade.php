@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <study-teams-list study-team-view-endpoint="{{$studyTeamViewEndpoint}}" study-teams-fetch-endpoint="{{$studyTeamsFetchEndpoint}}" ></study-teams-list>
    </div>
@endsection
