@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <study-team-view
            team-fetch-endpoint="{{$teamFetchEndpoint}}"
            course-view-endpoint="{{$courseViewEndpoint}}"
            course-types-endpoint="{{$courseTypesEndpoint}}"
            course-save-endpoint="{{$courseSaveEndpoint}}"
        ></study-team-view>
    </div>
@endsection
