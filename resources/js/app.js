/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue").default;

import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component(
    "example-component",
    require("./components/ExampleComponent.vue").default
);
Vue.component(
    "students-list",
    require("./components/students/list.vue").default
);
Vue.component(
    "study-teams-list",
    require("./components/studyTeams/list.vue").default
);
Vue.component(
    "study-team-view",
    require("./components/studyTeams/studyTeamView.vue").default
);
Vue.component(
    "course-view",
    require("./components/courses/courseView.vue").default
);
Vue.component("courses-list", require("./components/courses/list.vue").default);
Vue.component(
    "attendance-view",
    require("./components/attendances/view.vue").default
);
Vue.component(
    "attendances-list",
    require("./components/attendances/list.vue").default
);
Vue.component(
    "teacher-stats",
    require("./components/stats/teacherStats.vue").default
);
Vue.component(
    "student-dashboard",
    require("./components/dashboards/studentDashboard.vue").default
);
Vue.component(
    "teacher-dashboard",
    require("./components/dashboards/teacherDashboard.vue").default
);

Vue.component(
    "import-excel",
    require("./components/import/ImportExcel.vue").default
);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.use(Toast);

const app = new Vue({
    el: "#app"
});
