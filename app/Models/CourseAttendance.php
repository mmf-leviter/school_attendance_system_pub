<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CourseAttendance
 *
 * @property string $id
 * @property string $attendance_key
 * @property int $confirmed
 * @property string $started_at
 * @property string|null $completed_at
 * @property int $student_id
 * @property int $course_id
 * @property-read \App\Models\Course $course
 * @property-read \App\Models\User $student
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance query()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance whereAttendanceKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance whereCompletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance whereConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance whereStartedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance whereStudentId($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance whereUpdatedAt($value)
 */
class CourseAttendance extends Model
{
    use HasFactory;

    public $timestamps = false;
    public $incrementing = false;

    protected $fillable = [
        'id',
        'confirmed',
        'student_id',
        'course_id',
        'created_at',
        'updated_at',
        'attendance_key'
    ];

    protected $hidden = [
        'attendance_key',
    ];

    public function student(){
        return $this->belongsTo(User::class, 'student_id', 'id');
    }

    public function course() {
        return $this->belongsTo(Course::class, 'course_id', 'id');
    }

}
