<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Course
 *
 * @property int $id
 * @property int $course_type_id
 * @property string $start_time
 * @property string $end_time
 * @property int $time_limit
 * @property int $team_id
 * @property int $teacher_id
 * @property string $course_ip_address
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\CourseType $courseType
 * @property-read \App\Models\StudyTeam $studyTeam
 * @property-read \App\Models\StudyTeam $teacher
 * @method static \Illuminate\Database\Eloquent\Builder|Course newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Course newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Course query()
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereCourseTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseAttendance whereTimeLimit($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereCourseIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereTeacherId($value)
 */
class Course extends Model
{
    use HasFactory;

    const DEFAULT_TIME_LIMIT = 600;

    protected $fillable = [
        'course_type_id',
        'start_time',
        'end_time',
        'team_id',
        'teacher_id',
        'time_limit'
    ];

    public function courseType() {
        return $this->belongsTo(CourseType::class, 'course_type_id', 'id');
    }

    public function studyTeam() {
        return $this->belongsTo(StudyTeam::class, 'team_id', 'id');
    }

    public function teacher() {
        return $this->belongsTo(User::class, 'teacher_id', 'id');
    }
}
