<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\StudyTeam
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Course[] $courses
 * @property-read int|null $courses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $students
 * @property-read int|null $students_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $teachers
 * @property-read int|null $teachers_count
 * @method static \Illuminate\Database\Eloquent\Builder|StudyTeam newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StudyTeam newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|StudyTeam query()
 * @method static \Illuminate\Database\Eloquent\Builder|StudyTeam whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StudyTeam whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StudyTeam whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|StudyTeam whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class StudyTeam extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function students()
    {
        return $this->belongsToMany(
            User::class,
            'students_study_teams',
            'team_id',
            'student_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function teachers()
    {
        return $this->belongsToMany(
            User::class,
            'teachers_study_teams',
            'team_id',
            'teacher_id');
    }

    /**
     * returns the related courses
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courses() {
        return $this->hasMany(Course::class, 'team_id', 'id');
    }
}
