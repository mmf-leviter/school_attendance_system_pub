<?php

namespace App\Repositories;

use App\Models\CourseType;
use App\Repositories\BaseRepositoryContract as BaseRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CourseTypeRepository.
 */
class CourseTypeRepository implements BaseRepository
{

    /**
     * @var CourseType
     */
    protected $courseType;

    /**
     * CourseTypeRepository constructor.
     * @param $courseType
     */
    public function __construct(CourseType $courseType)
    {
        $this->courseType = $courseType;
    }

    /**
     * @return CourseType[]|Collection
     */
    public function all(){
        return $this->courseType->all();
    }

    /**
     * Get the coursetype by id
     * @param $id
     * @return CourseType
     */
    public function get($id): CourseType
    {
        return CourseType::find($id);
    }

    /**
     * @param $id
     * @return Model|void|NULL
     */
    public function getWithModels($id)
    {
        // TODO: Implement getWithModels() method.
    }

    /**
     * @return Collection|void
     */
    public function allWithModels()
    {
        // TODO: Implement allWithModels() method.
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|void
     */
    public function allWithModelsPaginated()
    {
        // TODO: Implement allWithModelsPaginated() method.
    }

    /**
     * @param array $data
     * @return bool|Collection|void
     */
    public function create(array $data)
    {
        // TODO: Implement create() method.
    }

    /**
     * @param int $id
     * @param array $data
     * @return Model|void
     */
    public function update(int $id, array $data)
    {
        // TODO: Implement update() method.
    }

    /**
     * @param int $id
     * @return bool|void
     */
    public function delete(int $id)
    {
        // TODO: Implement delete() method.
    }
}
