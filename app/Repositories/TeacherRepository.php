<?php


namespace App\Repositories;


use App\Models\User;
use App\Repositories\BaseRepositoryContract as BaseRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TeacherRepository
 * @package App\Repositories
 */
class TeacherRepository implements BaseRepository
{

    /**
     * @var User
     */
    protected $teacher;

    /**
     * CourseTypeRepository constructor.
     * @param User $teacher
     */
    public function __construct(User $teacher)
    {
        $this->teacher = $teacher;
    }

    /**
     * get the teacher
     * @param $id
     * @return mixed
     */
    public function get($id) {
        return $this->teacher->hasRole(['teacher'])->find($id);
    }

    /**
     * @param $id
     * @return Model|void|NULL
     */
    public function getWithModels($id)
    {
        // TODO: Implement getWithModels() method.
    }

    /**
     * @return Collection|void
     */
    public function all()
    {
        // TODO: Implement all() method.
    }

    /**
     * @return Collection|void
     */
    public function allWithModels()
    {
        // TODO: Implement allWithModels() method.
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|void
     */
    public function allWithModelsPaginated()
    {
        // TODO: Implement allWithModelsPaginated() method.
    }

    /**
     * @param array $data
     * @return bool|Collection|void
     */
    public function create(array $data)
    {
        // TODO: Implement create() method.
    }

    /**
     * @param int $id
     * @param array $data
     * @return Model|void
     */
    public function update(int $id, array $data)
    {
        // TODO: Implement update() method.
    }

    /**
     * @param int $id
     * @return bool|void
     */
    public function delete(int $id)
    {
        // TODO: Implement delete() method.
    }
}
