<?php


namespace App\Repositories;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface BaseRepositoryContract
{

    /**
     * Get Model by id.
     *
     * @param $id
     *
     * @return Model|NULL
     */
    public function get($id);

    /**
     * Get Model by id and eager load associated models.
     *
     * @param $id
     *
     * @return Model|NULL
     */
    public function getWithModels($id);

    /**
     * Get all.
     *
     * @return Collection
     */
    public function all();

    /**
     * Get all and eager load associated models.
     *
     * @return Collection
     */
    public function allWithModels();

    /**
     * Get all and eager load associated models and paginate the results.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function allWithModelsPaginated();

    /**
     * Create new Instance.
     *
     * @param array $data
     *
     * @return collection|boolean
     */
    public function create(array $data);

    /**
     * Update existing Instance.
     *
     * @param int   $id
     * @param array $data
     *
     * @return Model
     */
    public function update(int $id, array $data);

    /**
     * delete existing Instance.
     *
     * @param int   $id
     * @param array $data
     *
     * @return bool
     */
    public function delete(int $id);



}
