<?php

namespace App\Repositories;

use App\Models\StudyTeam;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\BaseRepositoryContract as BaseRepository;
//use Your Model

/**
 * Class StudyTeamRepository.
 */
class StudyTeamRepository implements BaseRepository
{

    /**
     * @var StudyTeam
     */
    protected $studyTeam;

    /**
     * StudyTeamRepository constructor.
     * @param $studyTeam
     */
    public function __construct(StudyTeam $studyTeam)
    {
        $this->studyTeam = $studyTeam;
    }

    /**
     * @param $id
     * @return Model|NULL
     */
    public function get($id) {
        return $this->studyTeam->find($id);
    }

    /**
     * @param $id
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function getWithTeachers($id) {
        return $this->studyTeam->with(['teachers'])->find($id);
    }

    /**
     * @param $teacherId
     * @return Builder[]|Collection
     */
    public function withRelationsByTeacher($teacherId)
    {
        return $this->studyTeam->with(['teachers','students', 'courses'])->whereHas('teachers', function($teacher) use ($teacherId) {
            $teacher->whereIn('teacher_id', array($teacherId));
        })->get();
    }

    /**
     * @param $id
     * @return Builder|Builder[]|Collection|Model|NULL
     */
    public function getWithModels($id)
    {
        return $this->studyTeam->with(['teachers', 'students', 'courses' => function($q) {
            $q->with(['courseType'])
                ->where(explode(" ", ('start_time'))[0], '>=', explode(" ", Carbon::now('2'))[0])
                ->orderBy('start_time', 'ASC')->get();
        }])->find($id);
    }

    /**
     * @return StudyTeam[]|Collection
     */
    public function all()
    {
        return StudyTeam::all();
    }

    /**
     * @return Builder[]|Collection
     */
    public function allWithModels()
    {
        return $this->studyTeam->with(['teachers', 'students', 'courses'])->get();
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|void
     */
    public function allWithModelsPaginated()
    {
        // TODO: Implement allWithModelsPaginated() method.
    }

    /**
     * @param array $data
     * @return bool|Collection|void
     */
    public function create(array $data)
    {
        // TODO: Implement create() method.
    }

    /**
     * @param int $id
     * @param array $data
     * @return Model|void
     */
    public function update(int $id, array $data)
    {
        // TODO: Implement update() method.
    }

    /**
     * @param int $id
     * @return bool|void
     */
    public function delete(int $id)
    {
        // TODO: Implement delete() method.
    }

    public function studyTeamsByTeacher($teacherId)
    {
        return StudyTeam::where('teacherStudyTeams', function($query) use ($teacherId) {
            $query->whereHas('teachers', function($teachers) use ($teacherId) {
                $teachers->where('users.id', $teacherId);
            });
        })->get();
    }
}
