<?php

namespace App\Repositories;

use App\Models\CourseAttendance;
use App\Models\StudyTeam;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Repositories\BaseRepositoryContract as BaseRepository;
//use Your Model

/**
 * Class CourseAttendanceRepository.
 */
class CourseAttendanceRepository implements BaseRepository
{
    /**
     * @var CourseAttendance
     */
    protected $courseAttendance;

    /**
     * CourseAttendanceRepository constructor.
     * @param $courseAttendance
     */
    public function __construct(CourseAttendance $courseAttendance)
    {
        $this->courseAttendance = $courseAttendance;
    }

    /**
     * @param $id
     * @return Model|NULL
     */
    public function get($id)
    {
        return CourseAttendance::with(['student', 'course' => function($course) {
            $course->with(['studyTeam', 'courseType'])->get();
        }])->whereId($id)->first();
    }

    /**
     * @param $id
     * @return Model|void|NULL
     */
    public function getWithModels($id)
    {
        // TODO: Implement getWithModels() method.
    }

    /**
     * @return Collection|void
     */
    public function all()
    {
        // TODO: Implement all() method.
    }

    /**
     * @return Builder[]|Collection
     */
    public function allWithModels()
    {
        return $this->courseAttendance->with([ 'student', 'course' => function($q){
            $q->with(['studyTeam'])->get();
        }])->get();
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|void
     */
    public function allWithModelsPaginated()
    {
        // TODO: Implement allWithModelsPaginated() method.
    }

    /**
     * @param array $data
     * @return bool|Collection|void
     */
    public function create(array $data)
    {
        // TODO: Implement create() method.
    }

    /**
     * @param $id
     * @param array $data
     * @return Model
     */
    public function update($id, array $data)
    {
        $model = CourseAttendance::find($id);
        $model->update($data);

        return $model;
    }

    /**
     * @param int $id
     * @return bool|void
     */
    public function delete(int $id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return $this->courseAttendance;
    }

    /**
     * Creates attendances for all students in related to a specific team and the course selected
     * @param $teamId
     * @param $courseId
     * @param int $timeLimit
     * @return bool
     */
    public function createAttendances($teamId, $courseId) {
        $studyTeam = StudyTeam::with('students')->find($teamId);
        if ($studyTeam) {
            DB::beginTransaction();
            foreach ($studyTeam->students as $student) {

                $createResponse = CourseAttendance::create(
                    [
                        'id' => Str::random(20),
                        'attendance_key' => Str::random(20),
                        'student_id' => $student->id,
                        'course_id' => $courseId,
                        'created_at' => Carbon::now()
                    ]
                );
                if (!$createResponse) {
                    DB::rollBack();
                    return false;
                }
            }
            DB::commit();
        }
        return true;
    }

    /**
     * @param int $studentDd
     * @return Builder[]|Collection
     */
    public function withRelationsByStudent(int $studentDd)
    {
        return $this->courseAttendance->with([ 'student', 'course' => function($q) {
            $q->with(['studyTeam', 'courseType'])->get();
        }])->where('student_id', $studentDd)->orderBy('created_at', 'desc')->get();
    }


}
