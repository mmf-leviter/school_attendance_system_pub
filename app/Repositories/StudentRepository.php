<?php


namespace App\Repositories;


use App\Models\Course;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class StudentRepository
implements BaseRepositoryContract
{

    public function get($id)
    {
        // TODO: Implement get() method.
    }

    public function getWithModels($id)
    {
        // TODO: Implement getWithModels() method.
    }

    public function all()
    {

    }

    public function studentsByTeacher($teacherId)
    {
        return User::whereHas('studentStudyTeams', function($query) use ($teacherId) {
            $query->whereHas('teachers', function($teachers) use ($teacherId) {
                $teachers->where('users.id', $teacherId);
            });
        })->get();
    }
    public function coursesByTeacher($teacherId)
    {
        return Course::where('teacher_id', $teacherId)->get();
    }

    public function allWithModels()
    {
        // TODO: Implement allWithModels() method.
    }

    public function allWithModelsPaginated()
    {
        // TODO: Implement allWithModelsPaginated() method.
    }

    public function create(array $data)
    {
        // TODO: Implement create() method.
    }

    public function update(int $id, array $data)
    {
        // TODO: Implement update() method.
    }

    public function delete(int $id)
    {
        // TODO: Implement delete() method.
    }
}
