<?php

namespace App\Repositories;

use App\Models\Course;
use Carbon\Carbon;
use App\Repositories\BaseRepositoryContract as BaseRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

//use Your Model

/**
 * Class CourseRepository.
 */
class CourseRepository implements BaseRepository
{
    /**
     * @var Course
     */
    protected $course;

    /**
     * CourseRepository constructor.
     * @param Course $course
     */
    public function __construct(Course $course)
    {
        $this->course = $course;
    }

    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return $this->course;
    }

    /**
     * @param $id
     * @return Model|NULL
     */
    public function get($id) {
        return $this->course->find($id);
    }

    /**
     * @param $teamId
     * @return string
     *  Return the records with relations
     */
    public function withRelationsByTeam($teamId)
    {
        return $this->course->with(['courseType', 'studyTeam'])->where('team_id', $teamId)->get();
    }

    public function withRelationsByTeacher($teacherId)
    {
        return $this->course->with(['courseType', 'studyTeam', 'teacher'])->whereHas('teacher', function($teacher) use ($teacherId) {
            $teacher->where('id', $teacherId);
        })->orderBy('start_time', 'desc')->get();

    }

    public function delete($id) {
        $course = Course::find($id);
        return $course->delete();
    }

    public function getWithModels($id)
    {
        return $this->course->with(['courseType', 'teacher', 'studyTeam' => function($studyTeam) use ($id) {
            $studyTeam->with(['students' => function($students) use ($id) {
                $students->with(['attendances' => function ($attendances) use ($id) {
                    $attendances->where('course_id', $id)->get();
                }])->get();
            }])->get();
        }])->find($id);
    }

    public function all()
    {
        // TODO: Implement all() method.
    }

    public function allWithModels()
    {
        return $this->course->with(['courseType', 'studyTeam'])->get();
    }

    public function allWithModelsPaginated()
    {
        // TODO: Implement allWithModelsPaginated() method.
    }

    public function create(array $data)
    {
        $course = new Course();
        $course->fill($data);

        if (isset($data['studyTeam'])) {
            $course->studyTeam()->associate($data['studyTeam']);
        }
        if (isset($data['courseType'])) {
            $course->courseType()->associate($data['courseType']);
        }
        if (isset($data['teacher'])) {
            $course->teacher()->associate($data['teacher']);
        }

        $course->save();
        return $course;
    }

    public function update(int $id, array $data)
    {
        $course = Course::find($id);
        $course->update($data);
        return $course;
    }




}
