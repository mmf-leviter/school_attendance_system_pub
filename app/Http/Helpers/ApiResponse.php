<?php


namespace App\Http\Helpers;


class ApiResponse {
    /**
     * Response.
     *
     * @var bool
     */
    public $response;

    /**
     * Msg from API Request.
     *
     * @var array
     */
    public $messages = [];

    /**
     * Data from API Request.
     *
     * @var object|NULL
     */
    public $data;

    /**
     * New model instance.
     *
     * @param bool $response
     *
     * @param array $messages
     * @param object|null $data should contain always an object (usually version of json_decode)
     */
    public function __construct(bool $response = TRUE, array $messages = [], object $data = NULL) {
        $this->response = $response;
        $this->messages = $messages;
        $this->data = $data;
    }

    /**
     * Check whether Request's Response was successful.
     *
     * @return bool
     */
    public function successful(): bool {
        return ($this->response);
    }
}
