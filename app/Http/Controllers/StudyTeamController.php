<?php

namespace App\Http\Controllers;

use App\Models\StudyTeam;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudyTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $data = [
            'studyTeamViewEndpoint' => route('study-team.show', [
                'id' => '_id_'
            ])
        ];
        if ($user->hasRole(['teacher'])) {
            $data['studyTeamsFetchEndpoint'] = route('api.studyTeams.allByTeacher');
        } elseif ($user->hasRole(['admin'])) {
            $data['studyTeamsFetchEndpoint'] = route('api.studyTeams');
        }


        return view('studyTeams.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $data = [
            'teamFetchEndpoint' => route('api.study-team.show', [
                'id' => $id
            ]),
            'courseViewEndpoint' => route('course.show', [
                'id' => '_id_'
            ]),
            'courseTypesEndpoint' => route('api.course-types'),
            'courseSaveEndpoint' => route('api.course.store')
        ];
        return view('studyTeams.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
}
