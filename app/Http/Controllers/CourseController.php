<?php

namespace App\Http\Controllers;

use App\Http\Helpers\ApiResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $data = [
            'courseViewEndpoint' => route('course.show', [
                'id' => '_id_'
            ]),
        ];
        if ($user->hasRole(['teacher'])) {
            $data['coursesFetchEndpoint'] = route('api.courses.allByTeacher');
        } elseif ($user->hasRole(['admin'])) {
            $data['coursesFetchEndpoint'] = route('api.courses');
        }


        return view('courses.index', $data);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $user = Auth::user();
        $data = [
            'user' => $user,
            'courseId' => $id,
            'attendanceUpdateEndpoint' => route('api.course-attendance.update', [
                'id' => '_id_'
            ]),
            'courseDeleteEndpoint' => route('api.course.delete', [
                'id' => $id
            ]),
            'attendanceStartEndpoint' => route('api.course-attendance.start'),
            'courseFetchEndpoint' => route('api.course.show', [
                'id' => $id
            ]),
            'courseUpdateEndpoint' => route('api.course.update', [
                'id' => $id
            ])
        ];
        return view('courses.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
}
