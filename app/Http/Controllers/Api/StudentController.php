<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiResponse;
use App\Models\User;
use App\Repositories\StudentRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    protected $studentRepository;

    /**
     * StudentController constructor.
     * @param StudentRepository $studentRepository
     */
    public function __construct(StudentRepository $studentRepository)
    {
        $this->studentRepository = $studentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $apiResponse = new ApiResponse();
        $students = User::with("roles")->whereHas("roles", function($q) {
            $q->whereIn("name", ["student"]);
        })->get();
        if (!$students) {
            $apiResponse->messages = ['no students where found'];
            $apiResponse->response = false;
            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $apiResponse->response = true;
        $apiResponse->data = $students;
        return response()->json($apiResponse, Response::HTTP_OK);
    }

    public function studentsByTeacher(Request $request){
        $apiResponse = new ApiResponse();

        $user = Auth::user();
        $students = $this->studentRepository->studentsByTeacher(($user->hasRole(['teacher']) ? $user->id : $request->get('teacher_id')));
        if (!$students) {
            $apiResponse->messages = ['no students where found'];
            $apiResponse->response = false;
            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $apiResponse->response = true;
        $apiResponse->data = $students;
        return response()->json($apiResponse, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
