<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiResponse;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $apiResponse = new ApiResponse();
        $teachers = User::with("roles")->whereHas("roles", function($q) {
            $q->whereIn("name", ["teacher"]);
        })->get();
        if (!$teachers) {
            $apiResponse->messages = ['no teachers where found'];
            $apiResponse->response = false;
            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $apiResponse->response = true;
        $apiResponse->data = $teachers;
        return response()->json($apiResponse, Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @param $id
     * @return JsonResponse
     */
    public function byTeam($id)
    {
        $apiResponse = new ApiResponse();

        $apiResponse->response = true;
        $apiResponse->data = '';
        return response()->json($apiResponse, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
