<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiResponse;
use App\Models\StudyTeam;
use App\Models\User;
use App\Repositories\StudyTeamRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class StudyTeamController extends Controller
{

    protected $studyTeamRepository;

    /**
     * StudyTeamController constructor.
     * @param $studyTeamRepository
     */
    public function __construct(StudyTeamRepository $studyTeamRepository)
    {
        $this->studyTeamRepository = $studyTeamRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $apiResponse = new ApiResponse();
        $teams = $this->studyTeamRepository->allWithModels();
        if (!$teams) {
            $apiResponse->response = false;
            $apiResponse->messages = ['could not find teams. Might be a server error'];
            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $apiResponse->response = true;
        $apiResponse->messages = ['Successfully found teams with relations'];
        $apiResponse->data = $teams;
        return response()->json($apiResponse, Response::HTTP_OK);

    }

    public function studyTeamsByTeacher(Request $request): \Illuminate\Http\JsonResponse
    {
        $apiResponse = new ApiResponse();
        $students = $this->studyTeamRepository->studyTeamsByTeacher($request->get('teacher_id'));
        if (!$students) {
            $apiResponse->messages = ['no students where found'];
            $apiResponse->response = false;
            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $apiResponse->response = true;
        $apiResponse->data = $students;
        return response()->json($apiResponse, Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource by teacher id.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function allByTeacher()
    {
        $apiResponse = new ApiResponse();
        $user = Auth::user();
        if (!$user->hasRole(['teacher'])) {
            $apiResponse->response = false;
            $apiResponse->messages = ['user does not have the rights to see this'];
            return response()->json($apiResponse, Response::HTTP_OK);
        }
        $teamsByTeacher = $this->studyTeamRepository->withRelationsByTeacher($user->id);
        if (!$teamsByTeacher) {
            $apiResponse->response = false;
            $apiResponse->messages = ['could not find teams. Might be a server error'];
            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $apiResponse->response = true;
        $apiResponse->messages = ['Successfully found teams with relations'];
        $apiResponse->data = $teamsByTeacher;
        return response()->json($apiResponse, Response::HTTP_OK);

    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $id)
    {
        $apiResponse = new ApiResponse();
        $team = $this->studyTeamRepository->getWithModels($id);
        if (!$team) {
            $apiResponse->response = false;
            $apiResponse->messages = ['not found'];
            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $apiResponse->response = true;
        $apiResponse->messages = ['found the team'];
        $apiResponse->data = $team;
        return response()->json($apiResponse, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
