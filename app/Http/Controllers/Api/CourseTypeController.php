<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiResponse;
use App\Repositories\CourseTypeRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CourseTypeController extends Controller
{
    /**
     * @var CourseTypeRepository
     */
    protected $courseTypeRepository;


    /**
     * CourseTypeController constructor.
     * @param CourseTypeRepository $courseTypeRepository
     */
    public function __construct(CourseTypeRepository $courseTypeRepository)
    {
        $this->courseTypeRepository = $courseTypeRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $apiResponse = new ApiResponse();

        $courseTypes = $this->courseTypeRepository->all();
        if (!$courseTypes) {
            $apiResponse->response = false;
            $apiResponse->messages = ['no course types where found - might be a server failure'];
            response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $apiResponse->response = true;
        $apiResponse->messages = ['Success! all course types where found'];
        $apiResponse->data = $courseTypes;

        return response()->json($apiResponse, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id)
    {
        $apiResponse = new ApiResponse();

        $courseType = $this->courseTypeRepository->get($id);
        if (!$courseType) {
            $apiResponse->response = false;
            $apiResponse->messages = ['no course type where found - might be a server failure'];
            response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $apiResponse->response = true;
        $apiResponse->messages = ['Success! Course type where found'];
        $apiResponse->data = $courseType;

        return response()->json($apiResponse, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
