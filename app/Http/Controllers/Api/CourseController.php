<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiResponse;
use App\Models\User;
use App\Repositories\CourseRepository;
use App\Repositories\CourseTypeRepository;
use App\Repositories\StudentRepository;
use App\Repositories\StudyTeamRepository;
use App\Repositories\TeacherRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Validator;

class CourseController extends Controller
{
    protected $courseRepository;
    protected $courseTypeRepository;
    protected $teacherRepository;
    protected $studyTeamRepository;
    protected $studentRepository;

    /**
     * CourseController constructor.
     * @param StudentRepository $studentRepository
     * @param StudyTeamRepository $studyTeamRepository
     * @param CourseRepository $courseRepository
     * @param CourseTypeRepository $courseTypeRepository
     * @param TeacherRepository $teacherRepository
     */
    public function __construct(StudentRepository $studentRepository, StudyTeamRepository $studyTeamRepository, CourseRepository $courseRepository, CourseTypeRepository $courseTypeRepository, TeacherRepository $teacherRepository)
    {
        $this->studyTeamRepository = $studyTeamRepository;
        $this->studentRepository = $studentRepository;
        $this->teacherRepository = $teacherRepository;
        $this->courseTypeRepository = $courseTypeRepository;
        $this->courseRepository = $courseRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $apiResponse = new ApiResponse();
        $courses = $this->courseRepository->allWithModels();
        if (!$courses) {
            $apiResponse->response = false;
            $apiResponse->messages = ['could not find courses. Might be a server error'];
            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $apiResponse->response = true;
        $apiResponse->messages = ['Successfully found courses with relations'];
        $apiResponse->data = $courses;
        return response()->json($apiResponse, Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource by teacher id.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function allByTeacher()
    {
        $apiResponse = new ApiResponse();
        $user = Auth::user();

        if (!$user->hasRole(['teacher'])) {
            $apiResponse->response = false;
            $apiResponse->messages = ['user does not have the rights to see this'];
            return response()->json($apiResponse, Response::HTTP_OK);
        }
        $coursesByTeacher = $this->courseRepository->withRelationsByTeacher($user->id);
        if (!$coursesByTeacher) {
            $apiResponse->response = false;
            $apiResponse->messages = ['could not find courses. Might be a server error'];
            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $apiResponse->response = true;
        $apiResponse->messages = ['Successfully found courses with relations'];
        $apiResponse->data = $coursesByTeacher;
        return response()->json($apiResponse, Response::HTTP_OK);

    }

    /**
     * Display a listing of the resource by team id.
     *
     * @param $teamId
     * @return JsonResponse
     */
    public function byTeam($teamId)
    {
        $apiResponse = new ApiResponse();
        $courses = $this->courseRepository->withRelationsByTeam($teamId);
        if (!$courses) {
            $apiResponse->response = false;
            $apiResponse->messages = ['could not find courses. Might be a server error'];
            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $apiResponse->response = true;
        $apiResponse->messages = ['Successfully found courses with relations'];
        $apiResponse->data = $courses;
        return response()->json($apiResponse, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $apiResponse = new ApiResponse();

        $validFields = $request->only([
            'course_type_id',
            'start_time',
            'end_time',
            'team_id',
            'teacher_id'
        ]);

        $createValidator = Validator::make($validFields, [
            'course_type_id'        => [
                'required',
                'integer',
            ],
            'start_time' => [
                'required',
                'date',
            ],
            'end_time'    => [
                'required',
                'date',
            ],
            'team_id' => [
                'required',
                'integer',
            ],
            'teacher_id' => [
                'required',
                'integer',
            ]
        ]);

        if ($createValidator->fails()) {

            $apiResponse->response = FALSE;
            $apiResponse->messages = array_merge([
                'Course create validation failed.'
            ], $createValidator->errors()
                ->all());

            return response()->json($apiResponse, Response::HTTP_BAD_REQUEST);
        }


        $courseType = $this->courseTypeRepository->get($validFields['course_type_id']);
        if (!$courseType) {
            $apiResponse->response = false;
            $apiResponse->messages = ['course type not found'];
            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);

        }
        $teacher = User::find($validFields['teacher_id']);
        if (!$teacher) {
            $apiResponse->response = false;
            $apiResponse->messages = ['teacher not found'];
            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        if (!$teacher->hasRole(['teacher'])) {
            $apiResponse->response = false;
            $apiResponse->messages = ['something went wrong'];
            return response()->json($apiResponse, Response::HTTP_BAD_REQUEST);
        }

        $studyTeam = $this->studyTeamRepository->getWithTeachers($validFields['team_id']);
        if (!$studyTeam) {
            $apiResponse->response = false;
            $apiResponse->messages = ['study team not found'];
            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $teacherValidate = $studyTeam->teachers()->find($validFields['teacher_id']);
        if (!$teacherValidate) {
            $apiResponse->response = false;
            $apiResponse->messages = ['Teacher does not belong to the team you are trying to create the course in'];
            return response()->json($apiResponse, Response::HTTP_BAD_REQUEST);
        }

        $courseData = [
            'courseType'    => $courseType,
            'start_time'        => $validFields['start_time'],
            'end_time'          => $validFields['end_time'],
            'studyTeam'           => $studyTeam,
            'teacher'        => $teacher
        ];

        $saveResponse = $this->courseRepository->create($courseData);
        if (!$saveResponse) {
            $apiResponse->response = false;
            $apiResponse->messages = ['could not store the course'];
            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);

        }
        $apiResponse->response = true;
        $apiResponse->messages = ['Course was created'];
        $apiResponse->data = $saveResponse;

        return response()->json($apiResponse, Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return string
     */
    public function show(int $id)
    {
        $apiResponse = new ApiResponse();
        $course = $this->courseRepository->getWithModels($id);
        if (!$course) {
            $apiResponse->response = false;
            $apiResponse->messages = ['not found'];
            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $apiResponse->response = true;
        $apiResponse->messages = ['found the course'];
        $apiResponse->data = $course;
        return response()->json($apiResponse, Response::HTTP_OK);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        $apiResponse = new ApiResponse();

        $validFields = $request->only([
            'start_time',
            'end_time',
            'teacher_id'
        ]);

        $updateValidator = Validator::make($validFields, [
            'start_time' => [
                'required',
                'date',
            ],
            'end_time'    => [
                'required',
                'date',
            ]
        ]);

        if ($updateValidator->fails()) {

            $apiResponse->response = FALSE;
            $apiResponse->messages = array_merge([
                'Course update validation failed.'
            ], $updateValidator->errors()
                ->all());

            return response()->json($apiResponse, Response::HTTP_BAD_REQUEST);
        }
        $courseData = [
            'start_time'        => $validFields['start_time'],
            'end_time'          => $validFields['end_time'],
        ];

        $course = $this->courseRepository->get($id);
        if (!$course) {
            $apiResponse->response = FALSE;
            $apiResponse->messages = array_merge([
                'Could not find the course'
            ]);

            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        if ($course && $course->course_ip_address) {
            $apiResponse->response = FALSE;
            $apiResponse->messages = array_merge([
                'Course is all ready started, so its not possible to update.'
            ]);

            return response()->json($apiResponse, Response::HTTP_LOCKED);
        }

        $updateResponse = $this->courseRepository->update($id, $courseData);
        if (!$updateResponse) {
            $apiResponse->response = false;
            $apiResponse->messages = ['could not update the course'];
            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);

        }
        $apiResponse->response = true;
        $apiResponse->messages = ['Course was updated'];
        $apiResponse->data = $updateResponse;

        return response()->json($apiResponse, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id)
    {
        $apiResponse = new ApiResponse();
        $course = $this->courseRepository->get($id);
        if (!$course) {
            $apiResponse->response = false;
            $apiResponse->messages = ['not found'];
            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $deleteResponse =$this->courseRepository->delete($id);

        if (!$deleteResponse) {
            $apiResponse->response = false;
            $apiResponse->messages = ['course was not deleted'];
            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $apiResponse->response = true;
        $apiResponse->messages = ['course was deleted'];
        $apiResponse->data = $deleteResponse;
        return response()->json($apiResponse, Response::HTTP_OK);
    }
}
