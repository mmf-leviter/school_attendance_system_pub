<?php

namespace App\Http\Controllers\Api;

use App\Events\CourseAttendanceUpdateEvent;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiResponse;
use App\Models\Course;
use App\Models\CourseAttendance;
use App\Repositories\CourseAttendanceRepository;
use App\Repositories\CourseRepository;
use App\Services\AttendanceService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CourseAttendanceController extends Controller
{
    /**
     * @var CourseAttendanceRepository
     */
    protected $courseAttendanceRepository;
    /**
     * @var CourseRepository
     */
    protected $courseRepository;

    /**
     * @var AttendanceService
     */
    protected $attendanceService;


    /**
     * CourseAttendanceController constructor.
     * @param CourseAttendanceRepository $courseAttendanceRepository
     * @param CourseRepository $courseRepository
     * @param AttendanceService $attendanceService
     */
    public function __construct(CourseAttendanceRepository $courseAttendanceRepository, CourseRepository $courseRepository, AttendanceService $attendanceService)
    {
        $this->courseRepository = $courseRepository;
        $this->courseAttendanceRepository = $courseAttendanceRepository;
        $this->attendanceService = $attendanceService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $apiResponse = new ApiResponse();
        $attendances = $this->courseAttendanceRepository->allWithModels();
        if (!$attendances) {
            $apiResponse->response = false;
            $apiResponse->messages = ['could not find attendances. Might be a server error'];
            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $apiResponse->response = true;
        $apiResponse->messages = ['Successfully found attendances with relations'];
        $apiResponse->data = $attendances;
        return response()->json($apiResponse, Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function studentAttendances()
    {
        $apiResponse = new ApiResponse();
        $attendances = $this->courseAttendanceRepository->withRelationsByStudent(Auth::user()->id);
        if (!$attendances) {
            $apiResponse->response = false;
            $apiResponse->messages = ['could not find attendances. Might be a server error'];
            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $apiResponse->response = true;
        $apiResponse->messages = ['Successfully found attendances with relations'];
        $apiResponse->data = $attendances;
        return response()->json($apiResponse, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createAttendances(Request $request){
        $apiResponse = new ApiResponse();

        $validation = $request->validate([
            'teamId' => 'integer|required',
            'courseId' => 'integer|required',
            'timeLimit' => 'integer'
        ]);

        if (!$validation) {
            $apiResponse->response = false;
            $apiResponse->messages = ['validation failed'];
            return response()->json($apiResponse, Response::HTTP_BAD_REQUEST);
        }
        DB::beginTransaction();
        $response = $this->courseAttendanceRepository->createAttendances($validation['teamId'], $validation['courseId']);
        if (!$response) {
            DB::rollBack();
            $apiResponse->response = false;
            $apiResponse->messages = ['could not create attendances'];
            return response()->json($apiResponse, Response::HTTP_BAD_REQUEST);

        }
        $course = Course::find($validation['courseId']);
        if ($course) {
            $course->course_ip_address = $request->ip();
            $course->time_limit = $validation['timeLimit'] ?? Course::DEFAULT_TIME_LIMIT;
            $course->save();
        } else {
            DB::rollBack();
            $apiResponse->response = false;
            $apiResponse->messages = ['could not create attendances'];
            return response()->json($apiResponse, Response::HTTP_BAD_REQUEST);
        }
        DB::commit();

        $apiResponse->response = true;
        $apiResponse->messages = ['attendances created successfully'];
        $apiResponse->data = $response;

        return response()->json($apiResponse, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(string $id)
    {
        $apiResponse = new ApiResponse();
        $getAttendanceResponse = $this->courseAttendanceRepository->get($id);
        if (!$getAttendanceResponse) {
            $apiResponse->response = false;
            $apiResponse->messages = ['not found'];
            return response()->json($apiResponse, Response::HTTP_BAD_REQUEST);
        }
        $apiResponse->response = true;
        $apiResponse->messages = ['found the attendance'];
        $apiResponse->data = $getAttendanceResponse;
        return response()->json($apiResponse, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, string $id)
    {
        $apiResponse = new ApiResponse();
        $data = $request->validate([
            'confirmed' => 'boolean|required',
            'attendanceKey' => 'string',
            'teacherValidate' => 'boolean'
        ]);

        $data['updated_at'] = Carbon::now();

        $attendance = $this->courseAttendanceRepository->get($id);
        if (!$attendance) {
            $apiResponse->response = false;
            $apiResponse->messages = ['not found'];
            return response()->json($apiResponse, Response::HTTP_BAD_REQUEST);

        }
        if (!$data['teacherValidate']) {
            if (strtoupper($attendance->attendance_key) != strtoupper($data['attendanceKey'])) {
                $apiResponse->response = false;
                $apiResponse->messages = array_merge(['the key does not match the course key'], [strtoupper($attendance->attendance_key), strtoupper($data['attendanceKey'])]);
                return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }

        $course = Course::find($attendance->course_id);
        if ($course) {
            if ($course->course_ip_address != $request->ip()) {
                $apiResponse->response = false;
                $apiResponse->messages = ['the ip location does not match the course location'];
                return response()->json($apiResponse, Response::HTTP_NOT_ACCEPTABLE);
            }
        }
        $updateResponse = $this->courseAttendanceRepository->update($id, $data);
        if (!$updateResponse) {
            $apiResponse->response = false;
            $apiResponse->messages = ['could not attend the student'];
            return response()->json($apiResponse, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $data = [
            'course' => $this->courseRepository->getWithModels($attendance->course_id),
            'attendance' => $attendance->refresh()
        ];
        CourseAttendanceUpdateEvent::broadcast(
            $attendance['course_id'],
            $data
        );
        $apiResponse->response = true;
        $apiResponse->messages = ['student attended'];
        $apiResponse->data = $updateResponse;

        return response()->json($apiResponse, Response::HTTP_OK);


        //
    }

    /**
     * will calculate statistics on a students attendance by team and
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStatisticAttendanceDataByTeam(Request $request) {
        $apiResponse = new ApiResponse();
        $data = $request->validate([
            'teamId' => 'integer|required',
            'courseTypeId' => 'integer|required',
        ]);
        $statistics = $this->attendanceService->getStatistics($data['teamId'], $data['courseTypeId']);
        if (!isset($statistics) || !$statistics || count($statistics) == 0) {
            $apiResponse->response = false;
            $apiResponse->messages = ['could not get statistics'];
            return response()->json($apiResponse, Response::HTTP_BAD_REQUEST);
        }

        $apiResponse->response = true;
        $apiResponse->messages = ['got statistics'];
        $apiResponse->data = [
            'statistics' => $statistics
        ];
        return response()->json($apiResponse, Response::HTTP_OK);
    }

    /**
     * @param $teamId
     */
    public function getStatisticAttendanceDataByStudent($teamId) {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
