<?php

namespace App\Http\Controllers;

use App\Repositories\CourseAttendanceRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Agent\Agent;
use function PHPUnit\Framework\at;

class CourseAttendanceController extends Controller
{

    protected $courseAttendanceRepository;

    /**
     * CourseAttendanceController constructor.
     * @param $courseAttendanceRepository
     */
    public function __construct(CourseAttendanceRepository $courseAttendanceRepository)
    {
        $this->courseAttendanceRepository = $courseAttendanceRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if (!$user->hasAnyRole(['student'])) {
            return redirect()->route('home')->withErrors(['message' => 'you dont have the rights to see this']);
        }
        $data = [
            'attendanceViewEndpoint' => route('attendance.show', [
                'id' => '_id_'
            ]),
            'attendancesFetchEndpoint' => route('api.student-attendances')
        ];



        return view('attendances.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param string $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function show(string $id)
    {
        $user = Auth::user();
        $agent = new Agent();
        if (!$user->hasAnyRole(['student'])) {
            return redirect()->route('home')->withErrors(['message' => 'you dont have the rights to see this']);
        }

        if ($agent->isMobile()) {
            return redirect()->route('attendances')->withErrors(['message' => 'please use a desktop']);
        }

        $attendance = $this->courseAttendanceRepository->get($id);

        if (!$attendance) {
            return redirect()->route('home')->withErrors(['message' => 'attendance does not exist']);
        }

        if ($attendance->student_id != $user->id) {
            return redirect()->route('home')->withErrors(['message' => 'you dont have the rights to see this']);
        }
        $data = [
            'attendanceFetchEndpoint' => route('api.course-attendance.get', [
                'id' => $id
            ]),
            'attendanceUpdateEndpoint' => route('api.course-attendance.update', [
                'id' => $id
            ])
        ];
        return view('attendances.show', $data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
}
