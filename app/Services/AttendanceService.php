<?php


namespace App\Services;


use App\Models\CourseAttendance;
use App\Repositories\CourseAttendanceRepository;

class AttendanceService
{

    protected $attendanceRepository;

    /**
     * AttendanceService constructor.
     * @param CourseAttendanceRepository $attendanceRepository
     */
    public function __construct(CourseAttendanceRepository $attendanceRepository)
    {
        $this->attendanceRepository = $attendanceRepository;
    }

    public function getStatistics($teamId = NULL, $courseTypeId = NULL)
    {
        $attendances = CourseAttendance::with(['course', 'student'])->whereHas('course', function ($q) use ($courseTypeId, $teamId) {
            $q->where('team_id', $teamId)->whereHas('courseType', function ($q) use ($courseTypeId) {
                $q->where('id', $courseTypeId);
            });
        })->orderBy('created_at')->get();

        if (!$attendances) {
            return false;
        }

        $statistics = [
            'labels' => [],
            'datasets' => [],
            'courses' => []
        ];
        foreach ($attendances as $attendance) {
            array_push($statistics['labels'], $attendance->course->start_time);
            $statistics['datasets'][$attendance->student->name][$attendance->course->start_time][$attendance->created_at] = $attendance->confirmed;

        }
        $statistics['labels'] = array_unique($statistics['labels'], SORT_REGULAR);

        foreach ( $statistics['datasets'] as $student => $statistic) {
            foreach ( $statistic as $date => $label) {
                $attendanceCount = 0;
                foreach ($label as $value) {
                    $attendanceCount += $value;
                }
                $studentAverage = round($attendanceCount / count($statistics['datasets'][$student][$date]) * 100, 2);
                $statistics['datasets'][$student][$date]['average'] = $studentAverage;
                if (isset($statistics['courses'][$date]['average'])) {
                    $statistics['courses'][$date]['average'] += $studentAverage / count($statistics['datasets']);
                } else {
                    $statistics['courses'][$date]['average'] = $studentAverage / count($statistics['datasets']);
                }
            }
        }


        return (array)$statistics;
    }
}
